package com.observerpattern.model;

import java.util.Observable;

/**
 * Created by tanvi on 20/12/17.
 */

public class UserDataRepository extends Observable {
    private static final UserDataRepository ourInstance = new UserDataRepository();
    private String mUserName;
    private int mAge;

    public static UserDataRepository getInstance() {
        return ourInstance;
    }

    private UserDataRepository() {
    }

    public int getmAge() {
        return mAge;
    }

    public void setmAge(int mAge) {
        this.mAge = mAge;
        setChanged();
        notifyObservers();
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String mUserName) {
        this.mUserName = mUserName;
        setChanged();
        notifyObservers();
    }
}
