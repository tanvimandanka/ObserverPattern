package com.observerpattern;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {

    private com.observerpattern.model.UserDataRepository mUserDataRepository;
    private TextView mFirstName;
    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mUserDataRepository.setUserName("tanvi mandanka");
            mUserDataRepository.setmAge(23);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFirstName = findViewById(R.id.txtFirstName);

        mUserDataRepository = com.observerpattern.model.UserDataRepository.getInstance();
        mUserDataRepository.addObserver(this);

        mUserDataRepository.setmAge(12);
        mUserDataRepository.setUserName("tanvi");
        mHandler.postDelayed(mRunnable, 2000);
    }

    @Override
    public void update(Observable observable, Object object) {
        if (observable instanceof com.observerpattern.model.UserDataRepository) {
            com.observerpattern.model.UserDataRepository userDataRepository = (com.observerpattern.model.UserDataRepository) observable;
            if (userDataRepository.getUserName()!=null)
            mFirstName.setText(userDataRepository.getUserName());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUserDataRepository.deleteObserver(this);
    }
}
